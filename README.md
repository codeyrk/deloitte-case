# README

WordReplacement API built in golang to deploy on AWS-EKS using aws-cdk and aws-cdk8s for infrastructure provisioning

----------

This project contains following folders:
## IaC/eks-cdk-go
This project deploys eks cluster programatically on AWS. Refer below diagrams for official architecture of EKS.

> EKS Architectural Overview:
https://www.eksworkshop.com/010_introduction/architecture/architecture_control_and_data_overview/

> EKS Control Plane:
https://www.eksworkshop.com/010_introduction/architecture/architecture_control/

> EKS Data Plane:
https://www.eksworkshop.com/010_introduction/architecture/architecture_worker/

## IaC/aws-cdk8s-go
This project deploys WordReplacement webservice on EKS.

## WordReplacement
This project serves as the web API for word replacement

---------

## How to use
> Guides for AWS CDK & CDK8S: 

> CDK: https://docs.aws.amazon.com/cdk/v2/guide/getting_started.html

> CDK8s: https://cdk8s.io/docs/latest/cli/installation/

- Install CDK
    ```sh
    $ npm install -g aws-cdk
    ```

- Install CDK8S
    ```sh
    $ npm i -g cdk8s-cli
    ```

- Clone this Repo
    ```sh
    git clone https://gitlab.com/codeyrk/deloitte-case.git
    ```

- Deploy eks on aws
    ```sh
    $ cd IaC/eks-cdk-go
    $ cdk bootstrap aws://${AWS_USER_ID}/${AWS_REGION}
    $ cdk synth
    $ cdk deploy
    ```
    Outputs:
    ```sh
    EksCdkGoStack.MyNewClusterConfigCommand1E0E0138 = aws eks update-kubeconfig --name my-new-cluster --region eu-central-1 --role-arn arn:aws:iam::378370054467:role/EksCdkGoStack-MyNewClusterMastersRoleEB8CEC7C-1JDBS5NV17RO
    EksCdkGoStack.MyNewClusterGetTokenCommand4893D1B1 = aws eks get-token --cluster-name my-new-cluster --region eu-central-1 --role-arn arn:aws:iam::378370054467:role/EksCdkGoStack-MyNewClusterMastersRoleEB8CEC7C-1JDBS5NV17RO
    Stack ARN:
    arn:aws:cloudformation:eu-central-1:378370054467:stack/EksCdkGoStack/6a928d80-281f-11ed-960e-02ea8482c578
    ```
    Run the command above to point your kubeconfig to new cluster.

- Build and publish WordReplacement container image to `registry.gitlab.com`
    ```sh
    $ cd WordReplacement
    $ docker build -t registry.gitlab.com/codeyrk/deloitte-case .
    $ docker push registry.gitlab.com/codeyrk/deloitte-case
    ```
- Deploy WordReplacement as WebService on EKS
    - Add secret to store registry access credentials in the cluster
    ```sh
    $ kubectl create secret docker-registry registry-secret --docker-username=${username} --docker-password=${password} --docker-server=registry.gitlab.com
    ```
    - Build Deployment
    ```sh
    $ cd IaC/aws-cdk8s-go
    $ cdk8s synth
    ```
    Outputs:

    ```sh
    ➜  aws-cdk8s-go git:(master) ✗ cdk8s synth           
    ------------------------------------------------------------------------------------------------
    A new version 2.0.100 of cdk8s-cli is available (current 2.0.99).
    Run "npm install -g cdk8s-cli" to install the latest version on your system.
    For additional installation methods, see https://cdk8s.io/docs/latest/getting-started
    ------------------------------------------------------------------------------------------------
    dist/wordreplacement-c8ae519a.k8s.yaml
    ```
    - Deploy the webservice
    ```sh
    $ kubectl apply -f dist/wordreplacement-c8ae519a.k8s.yaml
    ```
    Outputs:
    ```sh
    ➜  aws-cdk8s-go git:(master) ✗ kubectl apply -f dist/wordreplacement-c8ae519a.k8s.yaml 
    service/wordreplacement-service-c889b85c created
    deployment.apps/wordreplacement-deployment-c888d695 created
    ```

    Verify service and Pods
    ```sh
    ➜  aws-cdk8s-go git:(master) ✗ kubectl get pods
    NAME                                                  READY   STATUS    RESTARTS   AGE
    wordreplacement-deployment-c888d695-679788fc6-7w8pk   1/1     Running   0          12s
    wordreplacement-deployment-c888d695-679788fc6-lcrcx   1/1     Running   0          12s

    ➜  aws-cdk8s-go git:(master) ✗ kubectl get service -A
    NAMESPACE     NAME                               TYPE           CLUSTER-IP      EXTERNAL-IP                                                                  PORT(S)         AGE
    default       kubernetes                         ClusterIP      172.20.0.1      <none>                                                                       443/TCP         154m
    default       wordreplacement-service-c889b85c   LoadBalancer   172.20.92.220   a032cdae0173840088412e5b2c764055-1372791353.eu-central-1.elb.amazonaws.com   80:31863/TCP    30s
    kube-system   kube-dns                           ClusterIP      172.20.0.10     <none>                                                                       53/UDP,53/TCP   154m
    ```
    Our API end point: 
    `a032cdae0173840088412e5b2c764055-1372791353.eu-central-1.elb.amazonaws.com:80`

### Run API
Here is a sample API running example as per required functionality.
```sh
curl http://a032cdae0173840088412e5b2c764055-1372791353.eu-central-1.elb.amazonaws.com/replace \
    --include \
    --header "Content-Type: application/json" \
    --request "POST" \
    --data '{"input":"Deloitte, Microsoft, Google and Oracle"}'
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Date: Tue, 30 Aug 2022 07:40:10 GMT
Content-Length: 66

{
    "output": "Deloitte©, Microsoft©, Google© and Oracle©"
}%
```
