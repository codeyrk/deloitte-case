package main

import (
	"testing"
)

func TestWordReplacement(t *testing.T) {

	testData := []struct {
		input string
		want  string
	}{
		{"", ""},
		{"Oracl", "Oracl"},
		{"Oracle", "Oracle©"},
		{"Google", "Google©"},
		{"Microsoft", "Microsoft©"},
		{"Microsoft", "Microsoft©"},
		{"Amazon", "Amazon©"},
		{"Deloitte", "Deloitte©"},
		{"MicrosoftDeloitte", "Microsoft©Deloitte©"},
		{"Microsoft & Deloitte", "Microsoft© & Deloitte©"},
		{"Deloitte, Microsoft, Google, Amazon and Oracle", "Deloitte©, Microsoft©, Google©, Amazon© and Oracle©"},
	}

	for _, tc := range testData {

		replaceMap = make(map[string]string, 5)
		LoadStrings(&replaceMap)

		result := replaceAll(tc.input)
		if result != tc.want {
			t.Fatalf("Failed for %v, expected: %v, got: %v", tc.input, tc.want, result)
		} else {
			t.Logf("Success!")
		}

	}
}
