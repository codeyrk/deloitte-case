package main

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

// request represents
type request struct {
	Input string `json:"input"`
}

// request represents
type response struct {
	Output string `json:"output"`
}

var replaceMap map[string]string

// Load strings to be replaced
func LoadStrings(m *map[string]string) {
	(*m)["Oracle"] = "Oracle©"
	(*m)["Google"] = "Google©"
	(*m)["Microsoft"] = "Microsoft©"
	(*m)["Amazon"] = "Amazon©"
	(*m)["Deloitte"] = "Deloitte©"
}

// assign the handler function to an endpoint path.
func main() {

	replaceMap = make(map[string]string, 5)

	LoadStrings(&replaceMap)

	router := gin.Default()
	router.POST("/replace", replaceString)

	router.Run("0.0.0.0:8080")
}

// replaceString replaces string as Copyrighted string from JSON received in the request body.
func replaceString(c *gin.Context) {

	var req request

	// Call BindJSON to bind the received JSON to
	// req.
	if err := c.BindJSON(&req); err != nil {
		return
	}

	var res response
	res.Output = req.Input

	res.Output = replaceAll(res.Output)

	c.IndentedJSON(http.StatusCreated, res)
}

func replaceAll(input string) string {
	for src, dst := range replaceMap {
		input = strings.ReplaceAll(input, src, dst)
	}

	return input
}
